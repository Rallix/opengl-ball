/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import render.Loader;
import render.ObjLoader;
import textures.Texture2D;

/**
 *
 * @author Tom
 */
public class TexturedModel {

    private RawModel rawModel;
    private Texture2D texture;

    public TexturedModel(RawModel model, Texture2D texture) {
        this.rawModel = model;
        this.texture = texture;
    }
    
    public TexturedModel(Loader loader, String modelName, Texture2D texture) {
        this.rawModel = ObjLoader.loadObjModel(modelName, loader);
        this.texture = texture;
    }

    /**
     * Zjednodušený konstruktor pro načítání jednoduchých objektů
     */
    public TexturedModel(Loader loader, String modelName, String textureName) {
        this.rawModel = ObjLoader.loadObjModel(modelName, loader);
        this.texture = new Texture2D(loader.loadTexture(textureName));
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public Texture2D getTexture() {
        return texture;
    }

}
