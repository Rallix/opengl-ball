package engine;

import entities.Camera;
import entities.Coin;
import entities.Entity;
import entities.Light;
import entities.Player;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import models.RawModel;
import models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import java.awt.Font;
import org.newdawn.slick.TrueTypeFont;
import render.*;
import terrains.Terrain;
import textures.TerrainTexture;
import textures.TerrainTextures;
import textures.Texture2D;
import tools.Randomize;

/**
 *
 * @author Tom
 */
public class Game {

    public static final String GAME_TITLE = "Ball";

    public static final float AMBIENT_LIGHT_FACTOR = 0.2f;

    public static final int COIN_COUNT = 20;
    public static final int VEGETATION_COUNT = 500; // počet generovaných objektů vegetace
    public static final int ROCK_COUNT = 20; // počet skal
    public static final float WORLD_RIM = 10f; // okraj terénu, za nimiž už nejsou generovány objekty

    public static final String TERRAIN_TEXTURE = "grassy2";
    public static final String LAVA_TEXTURE = "Molten magma";

    private static boolean restartRequest = false;

    private static final float ORIGINAL_FOG_DENSITY = GlobalRenderer.fogDensity;
   

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Window.create();
        Loader loader = new Loader();
        
        // Fonty
        // Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
        // TrueTypeFont font = new TrueTypeFont(awtFont, false);´
        
        // Terén
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
        TerrainTexture redTexture = new TerrainTexture(loader.loadTexture("handpainted/water"));
        TerrainTexture greenTexture = new TerrainTexture(loader.loadTexture("pbr/Forest cobblestone (Albedo)"));
        TerrainTexture blueTexture = new TerrainTexture(loader.loadTexture("handpainted/sand"));
        TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("terrainBlendMap"));
        TerrainTextures textures = new TerrainTextures(backgroundTexture, redTexture, greenTexture, blueTexture);

        Terrain terrain = new Terrain(0, 0, loader, textures, blendMap, "hills");

        // Načíst všechny potřebné modely
        TexturedModel treeModel = new TexturedModel(loader, "tree", "tree");
        TexturedModel grassModel = new TexturedModel(
                ObjLoader.loadObjModel("grassModel", loader),
                new Texture2D(loader.loadTexture("grassTexture"), true, true) // průhledná textura
        );
        TexturedModel coinModel = new TexturedModel(loader, "Gold coin",
                new Texture2D(loader.loadTexture("pbr/Pure gold (Albedo)"), 2, 1) // odlesky
        );
        TexturedModel playerModel = new TexturedModel(loader, "Round ball",
                new Texture2D(loader.loadTexture("pbr/Shiny steel (Albedo)"), 0, 1) // odlesky
        );
        TexturedModel stallModel = new TexturedModel(loader, "stall", "stallTexture");
        TexturedModel well_topModel = new TexturedModel(loader, "Well_top", "pbr/Rough wood plank (Albedo)");
        TexturedModel well_bottomModel = new TexturedModel(loader, "Well_bottom", "Well");
        TexturedModel rock11Model = new TexturedModel(loader, "rock11_LP", "pbr/Rock (Albedo)");
        TexturedModel rock18Model = new TexturedModel(loader, "rock18_LP", "pbr/Cliff rock (Albedo)");
        TexturedModel mushroomModel = new TexturedModel(loader, "Mushroom", "Mushroom");

        // Rozmístit modely        
        List<Entity> entities = new ArrayList<>();
        Random random = new Random();
        float maxAngle = 360;
        for (int i = 0; i < VEGETATION_COUNT; i++) {
            // Stromy a tráva
            entities.add(new Entity(treeModel, Randomize.terrainPosition(random, terrain, WORLD_RIM), Randomize.verticalRotation(random, maxAngle), 3));
            entities.add(new Entity(grassModel, Randomize.terrainPosition(random, terrain, WORLD_RIM), Randomize.verticalRotation(random, maxAngle), 1));
            if (i % 10 == 0) entities.add(new Entity(mushroomModel, Randomize.terrainPosition(random, terrain, WORLD_RIM), Randomize.verticalRotation(random, maxAngle), 35));
        }
        for (int i = 0; i < ROCK_COUNT; i++) {
           int randomValue = random.nextInt();
           if (randomValue % 2 == 0) entities.add(new Entity(rock11Model, Randomize.terrainPosition(random, terrain, WORLD_RIM * 5f), Randomize.verticalRotation(random, maxAngle), 1));
           else entities.add(new Entity(rock18Model, Randomize.terrainPosition(random, terrain, WORLD_RIM * 5f), Randomize.verticalRotation(random, maxAngle), 1));
        }
        List<Coin> coins = new ArrayList<>();
        for (int i = 0; i < COIN_COUNT; i++) {
            Coin coin = new Coin(coinModel, Randomize.terrainPosition(random, terrain, WORLD_RIM), Randomize.verticalRotation(random, maxAngle), 2);
            coins.add(coin); // pro hráče
            entities.add(coin); // pro vykreslení
        }
        Entity stall = new Entity(stallModel, terrain.getCoordsPosition(0.65f, 0.30f, WORLD_RIM), new Vector3f(0,225,0), 2);
        Entity well_top = new Entity(well_topModel, terrain.getCoordsPosition(0.70f, 0.35f, WORLD_RIM), new Vector3f(0,0,0), 2);
        Entity well_bottom = new Entity(well_bottomModel, terrain.getCoordsPosition(0.70f, 0.35f, WORLD_RIM), new Vector3f(0,0,0), 2);

        Player player = new Player(playerModel, new Vector3f(Terrain.SIZE / 2, 0, Terrain.SIZE / 10 + 25), new Vector3f(0, -90, 0), 2, coins);
        Light sun = new Light(new Vector3f(20000, 20000, 2000), new Vector3f(1, 1, 1)); // white light
        //Light redLight = new Light(new Vector3f(-2000, 20000, -20000), new Vector3f(0.85f, 0.0f, 0.0f)); // Red light
        List<Light> lights = new ArrayList<>();
        lights.add(sun);
        //lights.add(redLight);
        Camera camera = new Camera(player);
        GlobalRenderer renderer = new GlobalRenderer();

        while (!(Display.isCloseRequested() || restartRequest)) {
            // klávesnice
            keyboardControls();
            // font.drawString(0,0, "HELLO!");
            // Animace entit zde
            Iterator<Coin> iter = coins.iterator();
            while (iter.hasNext()) {
                Coin coin = iter.next();
                coin.rotate();
                if (player.checkForCoin(coin)) {
                    // Nalezena mince                   
                    iter.remove();
                    entities.remove(coin);
                    System.out.println("Mince! Zbývá ještě " + coins.size());
                }
            }
            // Posun kamery a hráče
            camera.move();
            player.move(terrain);

            // Vykreslení : připravit všechny objekty k vykreslení
            renderer.processEntity(player);
            renderer.processEntity(stall);
            renderer.processEntity(well_top);
            renderer.processEntity(well_bottom);
            renderer.processEntities(entities);
            renderer.processTerrain(terrain);

            renderer.render(lights, camera); // vykreslit vše naráz            
            Window.update();
        }

        renderer.clean();
        loader.clean();
        Window.close();
        if (restartRequest) {
            restart();
        }
    }

    private static void keyboardControls() {
        if (Keyboard.isKeyDown(Keyboard.KEY_R)) {
            restartRequest = true;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_ADD)) {
            GlobalRenderer.fogDensity = Math.min(GlobalRenderer.fogDensity + ORIGINAL_FOG_DENSITY / 50f, 1);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT)) {
            GlobalRenderer.fogDensity = Math.max(0, GlobalRenderer.fogDensity - ORIGINAL_FOG_DENSITY / 50f);
        }

    }

    /**
     * Restartuje hru (znovu načte okno).
     */
    public static void requestRestart() {
        restartRequest = true;
    }

    private static void restart() {
        restartRequest = false;
        GlobalRenderer.fogDensity = ORIGINAL_FOG_DENSITY;
        main(new String[0]);
    }
}
