/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textures;

/**
 * Textura.
 *
 * @author Tom
 */
public class Texture2D {

    private int textureID;

    private int reflectivity = 0;
    private int shineDamper = 1;
    
    private boolean hasTransparency = false;
    private boolean useFakeLighting = false; // nastaví normály uměle

    public Texture2D(int id) {
        this(id, false, false);
    }
    
    public Texture2D(int id, boolean hasTransparency, boolean useFakeLighting) {
        this.textureID = id;
        this.hasTransparency = hasTransparency;
        this.useFakeLighting = useFakeLighting;
    }
    
    public Texture2D(int id, int reflectivity, int shineDamper) {
        this(id);
        this.reflectivity = reflectivity;
        this.shineDamper = shineDamper;
    }

    public int getID() {
        return this.textureID;
    }
    
    public boolean hasTransparency() {
        return hasTransparency;
    }   

    public boolean usingFakeLighting() {
        return useFakeLighting;
    }        

    public int getShineDamper() {
        return shineDamper;
    }

    public void setShineDamper(int shineDamper) {
        this.shineDamper = shineDamper;
    }

    public int getReflectivity() {
        return reflectivity;
    }

    public void setReflectivity(int reflectivity) {
        this.reflectivity = reflectivity;
    }

}
