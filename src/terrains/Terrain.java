package terrains;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import render.Loader;
import textures.TerrainTexture;
import textures.TerrainTextures;
import tools.Matrices;

/**
 *
 * @author Tom
 */
public class Terrain {

    /**
     * Rozměr čtvercového terénu.
     */
    public static final float SIZE = 800;
    /**
     * Nejnižší bod terénu.
     */
    public static final float LEVEL = 0;
    private static final float MAX_HEIGHT = 40;
    private static final float MAX_PIXEL_COLOUR = 256 * 256 * 256;

    private float[][] heights;

    private float length;
    private float width;
    private RawModel model;
    private TerrainTexture blendMap;
    private TerrainTextures textures;

    public Terrain(int gridX, int gridZ, Loader loader, TerrainTextures textures, TerrainTexture blendMap, String heightMap) {
        this.blendMap = blendMap;
        this.textures = textures;
        this.length = gridX * SIZE;
        this.width = gridZ * SIZE;

        this.model = generateTerrain(loader, heightMap); // plochý terén
    }

    public float getLength() {
        return length;
    }

    public float getWidth() {
        return width;
    }

    public RawModel getModel() {
        return model;
    }

    public TerrainTextures getTextures() {
        return textures;
    }

    public TerrainTexture getBlendMap() {
        return blendMap;
    }

    public float getHeightOfTerrain(float worldX, float worldZ) {
        float terrainX = worldX - this.length;
        float terrainZ = worldZ - this.width;

        float segmentSize = SIZE / ((float) heights.length - 1);

        // Na kterém segmentu mřížky se nacházíme
        int gridX = (int) Math.floor(terrainX / segmentSize);
        int gridZ = (int) Math.floor(terrainZ / segmentSize);

        if (gridX >= heights.length - 1 || gridZ >= heights.length - 1 || gridX < 0 || gridZ < 0) {
            // Neleží na terénu

            return 0;
        }
        float xCoord = (terrainX % segmentSize) / segmentSize; // X z rozmezí <0,1>
        float zCoord = (terrainZ % segmentSize) / segmentSize; // Z z rozmezí <0,1>
        float height = 0;
        // Na kterém z trojúhelníků ve čtverci hráč stojí
        if (xCoord <= (1 - zCoord)) {
            height = Matrices.barryCentric(
                    new Vector3f(0, heights[gridX][gridZ], 0),
                    new Vector3f(1, heights[gridX + 1][gridZ], 0),
                    new Vector3f(0, heights[gridX][gridZ + 1], 1),
                    new Vector2f(xCoord, zCoord));
        } else {
            height = Matrices.barryCentric(
                    new Vector3f(1, heights[gridX + 1][gridZ], 0),
                    new Vector3f(1, heights[gridX + 1][gridZ + 1], 1),
                    new Vector3f(0, heights[gridX][gridZ + 1], 1),
                    new Vector2f(xCoord, zCoord)
            );
        }
        return height;
    }

    /**
     * Generování trojúhelníkové sítě plochého terénu.
     *
     * @param loader Loader geometrických dat.
     * @return Model terénu.
     */
    private RawModel generateTerrain(Loader loader, String heightMap) {
        BufferedImage image = null;
        try {
            // TODO: Random heightmap
            image = ImageIO.read(new File("res/textures/heightmaps/" + heightMap + ".png"));
        } catch (IOException ex) {
            Logger.getLogger(Terrain.class.getName()).log(Level.SEVERE, null, ex);
        }

        int vertexCount = image.getHeight();

        heights = new float[vertexCount][vertexCount];

        int count = vertexCount * vertexCount;
        float[] vertices = new float[count * 3];
        float[] normals = new float[count * 3];
        float[] textureCoords = new float[count * 2];
        int[] indices = new int[6 * (vertexCount - 1) * (vertexCount - 1)];
        int vertexPointer = 0;
        for (int i = 0; i < vertexCount; i++) {
            for (int j = 0; j < vertexCount; j++) {
                vertices[vertexPointer * 3] = (float) j / ((float) vertexCount - 1) * SIZE;
                float height = getHeight(j, i, image);
                heights[j][i] = height;
                vertices[vertexPointer * 3 + 1] = height;
                vertices[vertexPointer * 3 + 2] = (float) i / ((float) vertexCount - 1) * SIZE;
                Vector3f normal = calculateNormal(j, i, image);
                normals[vertexPointer * 3] = normal.x;
                normals[vertexPointer * 3 + 1] = normal.y;
                normals[vertexPointer * 3 + 2] = normal.z;
                textureCoords[vertexPointer * 2] = (float) j / ((float) vertexCount - 1);
                textureCoords[vertexPointer * 2 + 1] = (float) i / ((float) vertexCount - 1);
                vertexPointer++;
            }
        }
        int pointer = 0;
        for (int gz = 0; gz < vertexCount - 1; gz++) {
            for (int gx = 0; gx < vertexCount - 1; gx++) {
                int topLeft = (gz * vertexCount) + gx;
                int topRight = topLeft + 1;
                int bottomLeft = ((gz + 1) * vertexCount) + gx;
                int bottomRight = bottomLeft + 1;
                indices[pointer++] = topLeft;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = topRight;
                indices[pointer++] = topRight;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = bottomRight;
            }
        }
        return loader.loadToVertexArrayObject(vertices, textureCoords, normals, indices);
    }

    /**
     * Vypočítá normálový vektor zvlněného terénu.
     */
    private Vector3f calculateNormal(int x, int z, BufferedImage image) {
        float heightL = getHeight(x - 1, z, image);
        float heightR = getHeight(x + 1, z, image);
        float heightD = getHeight(x, z - 1, image);
        float heightU = getHeight(x, z + 1, image);
        Vector3f normal = new Vector3f(heightL - heightR, 2f, heightD - heightU);
        normal.normalise();
        return normal;
    }

    /**
     * Vrací výšku určitého bodu z heightmapy.
     *
     */
    private float getHeight(int x, int z, BufferedImage image) {
        if (x < 0 || x >= image.getHeight() || z < 0 || z >= image.getWidth()) {
            // Mimo okraje mapy
            return 0;
        }
        float height = image.getRGB(x, z);
        height += MAX_PIXEL_COLOUR / 2f; // posun intervalu
        height /= MAX_PIXEL_COLOUR; // interval <-1, 1>
        height *= MAX_HEIGHT;
        return height;
    }
    
    /**
     * Vytvoří přesné souřadnice terénu z proporčních.
     * 
     * @param x Souřadnice délky (0 až 1).
     * @param z Souřadnice šířky (0 až 1).
     * @return Přesné souřadnice uvnitř terénu ve správné výšce
     */    
    public Vector3f getCoordsPosition(float x, float z, float rim) {
        float limit = rim / Terrain.SIZE;
        float min = limit;
        float max = 1 - limit;

        float randomX = min + x * (max - min);
        float randomZ = min + z * (max - min);

        float terrainX = randomX * Terrain.SIZE;
        float terrainZ = randomZ * Terrain.SIZE;
        float terrainY = getHeightOfTerrain(terrainX, terrainZ);

        return new Vector3f(terrainX, terrainY, terrainZ);
    }

}
