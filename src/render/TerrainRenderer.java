package render;

import entities.Entity;
import java.util.List;
import org.lwjgl.util.vector.Matrix4f;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import models.RawModel;
import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;
import shaders.TerrainShader;
import terrains.Terrain;
import textures.TerrainTextures;
import textures.Texture2D;
import tools.Matrices;

/**
 *
 * @author Tom
 */
public class TerrainRenderer {

    private TerrainShader shader;

    public TerrainRenderer(TerrainShader shader, Matrix4f projectionMatrix) {
        this.shader = shader;
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.connectTextureUnits();
        shader.stop();
    }

    public void render(List<Terrain> terrains) {
        for (Terrain terrain : terrains) {
            prepareTerrain(terrain);
            loadModelMatrix(terrain);

            // Vykreslení : typ, počet vrcholů, typ indexu, offset
            glDrawElements(GL_TRIANGLES, terrain.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
            unbindTextureModel();
        }
    }

    private void prepareTerrain(Terrain terrain) {
        RawModel rawModel = terrain.getModel();
        // VAO
        glBindVertexArray(rawModel.getVaoID()); // aktivace VAO
        glEnableVertexAttribArray(0); // povolení atributu 0 (pozice)
        glEnableVertexAttribArray(1); // povolení atributu 1 (textury)
        glEnableVertexAttribArray(2); // povolení atributu 2 (normály)
        // Textury
        bindTextures(terrain);
        shader.loadShine(1, 0);
    }

    private void bindTextures(Terrain terrain) {
        TerrainTextures textures = terrain.getTextures();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textures.getBackgroundTexture().getTextureID());

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textures.getRedTexture().getTextureID());

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textures.getGreenTexture().getTextureID());

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textures.getBlueTexture().getTextureID());

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());
    }

    private void unbindTextureModel() {
        // Uvolnění 
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glBindVertexArray(0);
    }

    private void loadModelMatrix(Terrain terrain) {
        // Transformace
        Matrix4f transformationMatrix = Matrices.createTransformationMatrix(
                new Vector3f(terrain.getLength(), 0, terrain.getWidth()),
                new Vector3f(0, 0, 0), // není třeba otáčet terénem
                1 // není třeba měnit velikost terénu
        );
        shader.loadTransformationMatrix(transformationMatrix);
    }
}
