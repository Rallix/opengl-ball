package render;

import models.RawModel;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * Načte geometrická data do vertex array objektů – a maže je při zavření okna.
 *
 * @author Tom
 */
public class Loader {

    private List<Integer> vertexArrayObjects = new ArrayList<>();
    private List<Integer> vertexBufferObjects = new ArrayList<>();
    private List<Integer> textures = new ArrayList<>();

    /**
     * Vytvoří VAO a načte do něj data na pozici 0.
     *
     * @param vertexPositions 3D pozice vrcholů
     * @return Načtený model
     */
    public RawModel loadToVertexArrayObject(float[] vertexPositions, float[] textureCoords, float[] normals, int[] indices) {
        int vaoID = createVertexArrayObject();
        bindIndicesBuffer(indices);

        storeDataInList(0, 3, vertexPositions); // 3: 3D trojúhelníky na pozici 0
        storeDataInList(1, 2, textureCoords); // 2: 2D trojúhelníky na pozici 1
        storeDataInList(2, 3, normals); // 3D normály

        unbindVertexArrayObject();
        return new RawModel(vaoID, indices.length);
    }

    /**
     * Načte texturu ze souboru formátu PNG.
     */
    public int loadTexture(String fileName) {
        Texture texture = null;
        try {
            texture = TextureLoader.getTexture("PNG", new FileInputStream("res/textures/" + fileName + ".png"));
            glGenerateMipmap(GL_TEXTURE_2D);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // Mipmapping
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.5f); // Oslabení efektu (zvýšení rozlišení)

        } catch (IOException ex) {
            Logger.getLogger(Loader.class.getName()).log(Level.SEVERE, null, ex);
        }
        int textureID = texture.getTextureID();
        textures.add(textureID);
        return textureID;
    }

    /**
     * Vytvoří v paměti VAO, aktivuje jej ("bind") a vrátí jeho ID.
     *
     * @return ID aktivního VAO.
     */
    private int createVertexArrayObject() {
        int vaoID = glGenVertexArrays();
        vertexArrayObjects.add(vaoID);

        glBindVertexArray(vaoID);
        return vaoID;
    }

    /**
     * Načte data do neměnného VBO, které vloží do VAO a nepotřebné VBO uvolní.
     *
     * @param attribNumber Číslo atributu ve VAO
     * @param data Data na načtení, zde pozice vrcholů
     */
    private void storeDataInList(int attribNumber, int coordinateSize, float[] data) {
        int vboID = glGenBuffers();
        vertexBufferObjects.add(vboID);

        glBindBuffer(GL_ARRAY_BUFFER, vboID);
        FloatBuffer buffer = storeDataInFloatBuffer(data);
        glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW); // read-only        

        // počet atributů, 3D vektory, typ dat, normalizováno,  vzdálenost mezi, offset
        glVertexAttribPointer(attribNumber, coordinateSize, GL_FLOAT, false, 0, 0);
        // unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    }

    private void bindIndicesBuffer(int[] indices) {
        int vboID = glGenBuffers();
        vertexBufferObjects.add(vboID);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
        IntBuffer buffer = storeDataInIntBuffer(indices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
    }

    private IntBuffer storeDataInIntBuffer(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    /**
     * Uloží data do float bufferu.
     *
     * @param data Float data pro uložení do bufferu
     * @return Float buffer pro načtení do VBO
     */
    private FloatBuffer storeDataInFloatBuffer(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data); // put data in the buffer
        buffer.flip(); // konec vkládání, příprava pro čtení
        return buffer;
    }

    /**
     * Uvolní VAO.
     */
    private void unbindVertexArrayObject() {
        glBindVertexArray(0); // 0 --> unbind
    }

    /**
     * Vyčistí všechnny VAO a VBO.
     */
    public void clean() {
        for (int vao : vertexArrayObjects) {
            glDeleteVertexArrays(vao);
        }
        for (int vbo : vertexBufferObjects) {
            glDeleteBuffers(vbo);
        }
        for (int texture : textures) {
            glDeleteTextures(texture);
        }
    }
}
