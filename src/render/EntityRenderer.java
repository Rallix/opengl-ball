/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package render;

import entities.Entity;
import java.util.List;
import java.util.Map;
import models.RawModel;
import models.TexturedModel;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import org.lwjgl.util.vector.Matrix4f;
import shaders.SurfaceShader;
import textures.Texture2D;
import tools.Matrices;

/**
 *
 * @author Tom
 */
public class EntityRenderer {

    private SurfaceShader shader;

    public EntityRenderer(SurfaceShader shader, Matrix4f projectionMatrix) {
        this.shader = shader;
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }

    public void render(Map<TexturedModel, List<Entity>> entities) {
        for (TexturedModel model : entities.keySet()) {
            prepareTextureModel(model);
            List<Entity> modelBatch = entities.get(model);
            for (Entity entity : modelBatch) {
                prepareInstance(entity);
                // Vykreslení : typ, počet vrcholů, typ indexu, offset
                glDrawElements(GL_TRIANGLES, model.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0);
            }
            unbindTextureModel();
        }
    }

    private void prepareTextureModel(TexturedModel model) {
        RawModel rawModel = model.getRawModel();
        // VAO
        glBindVertexArray(rawModel.getVaoID()); // aktivace VAO
        glEnableVertexAttribArray(0); // povolení atributu 0 (pozice)
        glEnableVertexAttribArray(1); // povolení atributu 1 (textury)
        glEnableVertexAttribArray(2); // povolení atributu 2 (normály)
        // Textury
        Texture2D texture = model.getTexture();
        if (texture.hasTransparency()) GlobalRenderer.enableCulling(false);        
        shader.loadShine(texture.getShineDamper(), texture.getReflectivity());
        shader.loadFakeLighting(texture.usingFakeLighting());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture.getID());
    }

    private void unbindTextureModel() {
        // Uvolnění
        GlobalRenderer.enableCulling(true);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glBindVertexArray(0);
    }

    private void prepareInstance(Entity entity) {
        // Transformace
        Matrix4f transformationMatrix = Matrices.createTransformationMatrix(
                entity.getPosition(),
                entity.getRotation(),
                entity.getScale()
        );
        shader.loadTransformationMatrix(transformationMatrix);
    }
}
