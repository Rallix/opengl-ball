package render;

import engine.Game;
import org.newdawn.slick.Color;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.opengl.ContextAttribs;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.TrueTypeFont;

/**
 *
 * @author Tom
 */
public class Window {

    private static final int WIDTH = 1920;
    private static final int HEIGHT = 1280;
    private static final int MAX_FPS = 60;

    private static long lastFrameTime;
    private static float delta;

    private static TrueTypeFont font;
    private static Font awtFont;

    public static void drawString(int x, int y, String text) {
        font.drawString(x, y, text);
    }

    public static void create() {
        //awtFont = new Font("Times New Roman", Font.BOLD, 24);
        //font = new TrueTypeFont(awtFont, false);

        ContextAttribs attribs = new ContextAttribs(3, 2)
                .withForwardCompatible(true)
                .withProfileCore(true);

        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create(new PixelFormat(), attribs);
            Display.setTitle(Game.GAME_TITLE);
        } catch (LWJGLException ex) {
            Logger.getLogger(Window.class.getName()).log(Level.SEVERE, null, ex);
        }

        glViewport(0, 0, WIDTH, HEIGHT);
        lastFrameTime = getCurrentTime();
    }

    public static void update() {
        Display.sync(MAX_FPS);
        Display.update();

        long currentFrameTime = getCurrentTime();
        delta = (currentFrameTime - lastFrameTime) / 1000f;
        lastFrameTime = currentFrameTime;
    }

    public static void close() {
        Display.destroy();
    }

    public static float getFrameTimeSeconds() {
        return delta;
    }

    private static long getCurrentTime() {
        return Sys.getTime() * 1000 / Sys.getTimerResolution();
    }

}
