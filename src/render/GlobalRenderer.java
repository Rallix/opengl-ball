/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package render;

import entities.Camera;
import entities.Entity;
import entities.Light;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.lwjgl.opengl.GL11.*;
import models.TexturedModel;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import shaders.SurfaceShader;
import shaders.TerrainShader;
import terrains.Terrain;

/**
 * Optimalizace vykreslování pro mnohonásobné vykreslování téhož objektu.
 *
 * @author Tom
 */
public class GlobalRenderer {

    private static final float FIELD_OF_VIEW = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;
    
    public static Vector3f SKY_COLOUR = new Vector3f(0.5f, 0.5f, 0.5f);
    public static float fogDensity = 0.007f;

    private Matrix4f projectionMatrix;

    private SurfaceShader shader;
    private EntityRenderer renderer;

    private TerrainRenderer terrainRenderer;
    private TerrainShader terrainShader;

    private Map<TexturedModel, List<Entity>> entities = new HashMap<>();
    private List<Terrain> terrains = new ArrayList<>();

    public GlobalRenderer() {
        enableCulling(true);

        projectionMatrix = createProjectionMatrix();
        shader = new SurfaceShader();
        renderer = new EntityRenderer(shader, projectionMatrix);

        terrainShader = new TerrainShader();
        terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
    }

    /**
     * Zapne backface culling.
     */
    public static void enableCulling(boolean active) {
        if (active) {
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    public void render(List<Light> lights, Camera camera) {
        prepare();

        shader.start();
        shader.loadSkyColour(SKY_COLOUR.x, SKY_COLOUR.y, SKY_COLOUR.z);
        shader.loadFogDensity(fogDensity);
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);
        renderer.render(entities);
        shader.stop();

        terrainShader.start();
        terrainShader.loadSkyColour(SKY_COLOUR.x, SKY_COLOUR.y, SKY_COLOUR.z);
        terrainShader.loadFogDensity(fogDensity);
        terrainShader.loadLight(lights.get(0)); // TODO: List of lights
        terrainShader.loadViewMatrix(camera);
        terrainRenderer.render(terrains);
        terrainShader.stop();

        terrains.clear();
        entities.clear();
    }

    /**
     * Příprava pro render každý frame (červená obrazovka).
     */
    public void prepare() {
        // zjistit, které trojúhelníky by se měly vykreslit dříve
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // vyčistit oba buffery
        glClearColor(SKY_COLOUR.x, SKY_COLOUR.y, SKY_COLOUR.z, 1);
    }

    /**
     * Zpracuje entitu pro pozdější vykreslení – seskupí objekty stejného druhu.
     *
     * @param entity
     */
    public void processEntity(Entity entity) {
        TexturedModel model = entity.getModel();
        List<Entity> batchModels = entities.get(model);
        if (batchModels != null) {
            // Už se chystáme nějaký model vykreslit
            batchModels.add(entity);
        } else {
            // První objekt svého druhu
            List<Entity> newBatch = new ArrayList<>();
            newBatch.add(entity);
            entities.put(model, newBatch);
        }
    }

    /**
     * Zpracuje seznam entit pro pozdější vykreslení.
     *
     * @param entities Seznam entit k vykreslení.
     */
    public void processEntities(List<Entity> entities) {
        for (Entity entity : entities) {
            processEntity(entity);
        }
    }

    /**
     * Vloží terén do seznamu.
     *
     * @param terrain Terén.
     */
    public void processTerrain(Terrain terrain) {
        terrains.add(terrain);
    }

    /**
     * Vytvoří projekční matici z definovaných konstant.
     */
    private Matrix4f createProjectionMatrix() {
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float y_scale = (float) ((1f / Math.tan(Math.toRadians(FIELD_OF_VIEW / 2f))) * aspectRatio);
        float x_scale = y_scale / aspectRatio;
        float frustum_length = FAR_PLANE - NEAR_PLANE;

        Matrix4f projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = x_scale;
        projectionMatrix.m11 = y_scale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
        projectionMatrix.m33 = 0;

        return projectionMatrix;
    }

    public void clean() {
        shader.clean();
        terrainShader.clean();
    }
}
