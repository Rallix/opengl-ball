/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import engine.Game;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.openal.SoundStore;
import org.newdawn.slick.util.ResourceLoader;
import render.Window;
import terrains.Terrain;

/**
 *
 * @author Tom
 */
public class Player extends Entity {

    private static final float GRAVITY = 9.81f;
    private static final float GRAVITY_MULTIPLIER = 10f;

    private static final float RUN_SPEED = 60;
    private static final float TURN_SPEED = 160;
    private static final float JUMP_POWER = 50;

    private float currentSpeed = 0;
    private float currentTurnSpeed = 0;
    private float upwardsVelocity = 0;

    private boolean isInAir = false;

    private static final float COIN_DISTANCE_GRAB = 2.5f;
    private static final String COIN_NAME = "8BIT_RETRO_Coin_Collect_Two_Dual_Note_Fast_Twinkle_mono";
    private static final String VICTORY_NAME = "Game Organic Magic Poof Buff Hit";
    private Audio coinBleep;
    private Audio victoryCheer;

    private int score = 0;


    List<Coin> coins;

    public Player(TexturedModel model, Vector3f position, Vector3f rotation, float scale, List<Coin> coins) {
        super(model, position, rotation, scale);
        this.coins = coins;
        loadSounds();
    }

    public void move(Terrain terrain) {
        keyboardControls();
        float lastFrameTime = Window.getFrameTimeSeconds();

        super.rotate(0, currentTurnSpeed * lastFrameTime, 0);
        float distance = currentSpeed * lastFrameTime;

        // Výpočet změny pozice hráče
        float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotation().y)));
        // Gravitace : xf = x0 + v0*t + (1/2)*g*t^2
        float dy = (float) (upwardsVelocity * lastFrameTime + (GRAVITY / 2) * Math.pow(lastFrameTime, 2));
        upwardsVelocity -= GRAVITY * GRAVITY_MULTIPLIER * lastFrameTime;

        float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotation().y)));

        super.move(dx, dy, dz);        
        Vector3f newPosition = super.getPosition();                        

        float terrainHeight = terrain.getHeightOfTerrain(newPosition.x, newPosition.z);
        
        if (newPosition.y < terrainHeight) {
            upwardsVelocity = 0;
            isInAir = false;
            super.getPosition().y = terrainHeight;
        }
        if (newPosition.x <= 0 || newPosition.x >= Terrain.SIZE) {
            // Mimo X souřadnici terénu
            super.getPosition().x = Math.max(0, Math.min(newPosition.x, Terrain.SIZE)); // Clamps the value
        }
        if (newPosition.z <= 0 || newPosition.z >= Terrain.SIZE) {
            // Mimo Z souřadnici terénu
            super.getPosition().z = Math.max(0, Math.min(newPosition.z, Terrain.SIZE)); // Clamps the value
        }
    }

    private void jump() {
        if (!isInAir) {
            isInAir = true;
            this.upwardsVelocity = JUMP_POWER;
        }
    }

    private void loadSounds() {
        try {
            coinBleep = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/" + COIN_NAME + ".wav"));
            victoryCheer = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/" + VICTORY_NAME + ".wav"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sebere minci, pokud je v dostatečně malé vzdálenosti.
     *
     * @param coin Zpracovávaná mince
     * @return Mince je dostatečně blízko.
     */
    public boolean checkForCoin(Coin coin) {
        if (withinDistance(coin.getPosition())) {
            grabCoin(coin);
            return true;
        } else {
            return false;
        }
    }

    private void grabCoin(Coin coin) {
        score++;
        coinBleep.playAsSoundEffect(1, 1, false); // Zvuk
        if (score >= Game.COIN_COUNT) {
            // Vítězství
            victoryCheer.playAsSoundEffect(1, 1, false);
        }
        SoundStore.get().poll(0); // polling
    }

    /**
     * Zjistí, zda jsou k sobě dva body dostatečně blízko.
     */
    public boolean withinDistance(Vector3f position) {
        // (a - b).length
        Vector3f playerPosition = getPosition();
        Vector3f vector = new Vector3f(
                playerPosition.x - position.x,
                playerPosition.y - position.y,
                playerPosition.z - position.z
        );
        float distance = (float) Math.sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
        return distance <= COIN_DISTANCE_GRAB;
    }

    private void keyboardControls() {
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            this.currentSpeed = RUN_SPEED;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            this.currentSpeed = -RUN_SPEED;
        } else {
            this.currentSpeed = 0;
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            this.currentTurnSpeed = TURN_SPEED;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            this.currentTurnSpeed = -TURN_SPEED;
        } else {
            this.currentTurnSpeed = 0;
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            jump();
        }
    }
}
