/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;
import render.Window;

/**
 *
 * @author Tom
 */
public class Coin extends Entity {
    
    private static final float TURN_SPEED = 160;
    
    public Coin(TexturedModel model, Vector3f position, Vector3f rotation, float scale) {
        super(model, position, rotation, scale);
    }
    
    /**
     * Točící se mince.
     */
    public void rotate() {
        float lastFrameTime = Window.getFrameTimeSeconds();
        super.rotate(0, TURN_SPEED * lastFrameTime, 0);
    }
    
}
