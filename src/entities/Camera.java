package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import terrains.Terrain;

/**
 *
 * @author Tom
 */
public class Camera {

    private float distanceFromPlayer = 50;
    private float angleAroundPlayer = 0;

    private Vector3f position = new Vector3f(Terrain.SIZE / 2, 15, Terrain.SIZE / 10);
    private float pitch = 10; // up and down
    private float yaw = -180; // left and right
    private float roll; // tilt

    public static final float MOVE_INC = 1f;
    public static final float TILT_INC = 2f;

    public static final int MOUSE_LEFT = 0;
    public static final int MOUSE_RIGHT = 1;

    private Player player;

    public Camera(Player player) {
        this.player = player;
    }
    
    public void move() {
        calculateZoom();
        calculatePitch();
        calculateAngle();

        float horizontal = calculateHorizontalDistance();
        float vertical = calculateVerticalDistance();
        calculateCameraPosition(horizontal, vertical);
        this.yaw = 180 - (player.getRotation().y + angleAroundPlayer);
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    private void calculateCameraPosition(float horizontal, float vertical) {
        float theta = player.getRotation().y + angleAroundPlayer; // úhel kamery od osy
        float offsetX = (float) (horizontal * Math.sin(Math.toRadians(theta)));
        float offsetZ = (float) (horizontal * Math.cos(Math.toRadians(theta)));
        Vector3f playerPosition = player.getPosition();
        position.x = playerPosition.x - offsetX;
        position.z = playerPosition.z - offsetZ;
        position.y = playerPosition.y + vertical;
    }

    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance() {
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    /**
     * Přiblíží a oddálí kameru od hráče v závislosti na kolečku myši.
     */
    private void calculateZoom() {
        float zoom = Mouse.getDWheel() * 0.1f;
        distanceFromPlayer -= zoom;
    }

    /**
     * Zvedne a sklopí kameru od hráče při držení pravého tlačítka myši.
     */
    private void calculatePitch() {
        if (Mouse.isButtonDown(MOUSE_RIGHT)) {
            // Right mouse button
            float dPitch = Mouse.getDY() * 0.1f;
            pitch -= dPitch;
        }
    }

    private void calculateAngle() {
        if (Mouse.isButtonDown(MOUSE_RIGHT)) {
            // Right mouse button
            float dAngle = Mouse.getDX() * 0.3f;
            angleAroundPlayer -= dAngle;
        }
    }

}
