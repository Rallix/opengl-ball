package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;

/**
 * Instance TexturedModel s informacemi o transformacích.
 *
 * @author Tom
 */
public class Entity {

    protected TexturedModel model;

    protected Vector3f position;
    protected Vector3f rotation;
    protected float scale;

    public Entity(TexturedModel model, Vector3f position, Vector3f rotation, float scale) {
        this.model = model;
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    /**
     * Posun entity po osách.
     *
     * @param posX Posun po ose X.
     * @param posY Posun po ose Y.
     * @param posZ Posun po ose Z.
     */
    public void move(float posX, float posY, float posZ) {
        // Sečte současnou pozici a posun, což vloží do staré pozice
        Vector3f.add(this.position, new Vector3f(posX, posY, posZ), this.position);
    }

    /**
     * Rotace entity po osách.
     *
     * @param rotX Rotace po ose X.
     * @param rotY Rotace po ose Y.
     * @param rotZ Rotace po ose Z.
     */
    public void rotate(float rotX, float rotY, float rotZ) {
        // Sečte současnou rotaci a novou, což vloží do staré rotace
        Vector3f.add(this.rotation, new Vector3f(rotX, rotY, rotZ), this.rotation);
    }

    public TexturedModel getModel() {
        return model;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

}
