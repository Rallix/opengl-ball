#version 430 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;
in float visibility;

out vec4 out_colour;

// Textury terénu
uniform sampler2D backgroundTexture;
uniform sampler2D redTexture;
uniform sampler2D blueTexture;
uniform sampler2D greenTexture;
uniform sampler2D blendMap;

uniform sampler2D modelTexture;
// difuzní světlo
uniform vec3 lightColour;
// spekulární světlo
uniform float shineDamper;
uniform float reflectivity; 
// barva oblohy (mlha)
uniform vec3 skyColour;


void main(void) {
    
    // Blend mapping
    vec4 blendMapColour = texture(blendMap, pass_textureCoords); // rozvržení textur terénu

    float backgroundTextureAmount = 1 - (blendMapColour.r + blendMapColour.g + blendMapColour.b); // základní textura pozadí 
    vec2 tiledCoords = pass_textureCoords * 40;
    vec4 backgroundTextureColour = texture(backgroundTexture, tiledCoords) * backgroundTextureAmount;
    vec4 redTextureColour = texture(redTexture, tiledCoords) * blendMapColour.r;
    vec4 greenTextureColour = texture(greenTexture, tiledCoords) * blendMapColour.g;
    vec4 blueTextureColour = texture(blueTexture, tiledCoords) * blendMapColour.b;

    vec4 finalColour = backgroundTextureColour + redTextureColour + greenTextureColour + blueTextureColour;


    vec3 unitNormal = normalize(surfaceNormal); // pouze směr normály
    vec3 unitToLight = normalize(toLightVector); // pouze směr ke světlu    
    float normalDotlight = dot(unitNormal, unitToLight); // vzdálenost směrů od sebe
    float ambientLightFactor = 0.2f;
    float brightness = max(normalDotlight, ambientLightFactor); // vždy alespoň ambientní světlo
    vec3 diffuse = brightness * lightColour; // barva
    
    vec3 unitToCamera = normalize(toCameraVector); // pouze směr ke kameře
    vec3 lightDirection = -unitToLight; // pouze směr od světla
    vec3 reflectedDirection = reflect(lightDirection, unitNormal); // směr odraženého světla

    float specularFactor = dot(reflectedDirection, unitToCamera);
    specularFactor = max(specularFactor, 0); // větší než nula
    float dampedFactor = pow(specularFactor, shineDamper);
    vec3 specular = dampedFactor * reflectivity * lightColour; // odlesky

    out_colour = vec4(diffuse, 1) * finalColour + vec4(specular, 1);
    out_colour = mix(vec4(skyColour, 1), out_colour, visibility); // mlha
}