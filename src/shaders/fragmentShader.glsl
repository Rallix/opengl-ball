#version 430 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector[2];
in vec3 toCameraVector;
in float visibility;

out vec4 out_colour;

const float ambientLight = 0.2f;

uniform sampler2D modelTexture;
// difuzní světlo
uniform vec3 lightColour[2];
// spekulární světlo
uniform float shineDamper;
uniform float reflectivity; 
// barva oblohy (mlha)
uniform vec3 skyColour;


void main(void) {
    vec3 unitNormal = normalize(surfaceNormal); // pouze směr normály
    vec3 unitToCamera = normalize(toCameraVector); // pouze směr ke kameře

    vec3 totalDiffuse = vec3(0.0);
    vec3 totalSpecular = vec3(0.0);

    for (int i = 0; i < 2; i++) {
        vec3 unitToLight = normalize(toLightVector[i]); // pouze směr ke světlu    
        float normalDotlight = dot(unitNormal, unitToLight); // vzdálenost směrů od sebe
        float brightness = max(normalDotlight, 0);    // větší než nula    
        vec3 lightDirection = -unitToLight; // pouze směr od světla
        vec3 reflectedDirection = reflect(lightDirection, unitNormal); // směr odraženého světla
        float specularFactor = dot(reflectedDirection, unitToCamera);
        specularFactor = max(specularFactor, 0); // větší než nula
        float dampedFactor = pow(specularFactor, shineDamper);
        vec3 diffuse = brightness * lightColour[i]; // barva
        vec3 specular = dampedFactor * reflectivity * lightColour[i]; // odlesky
    }
    
    totalDiffuse = max(totalDiffuse, ambientLight); // vždy alespoň ambientní světlo    

    vec4 textureColor = texture(modelTexture, pass_textureCoords); // základní barva z textury

    if (textureColor.a < 0.5) {
        discard; // zahodit průhledné fragmenty
    }

    out_colour = vec4(totalDiffuse, 1) * textureColor + vec4(totalSpecular, 1);
    out_colour = mix(vec4(skyColour, 1), out_colour, visibility); // mlha
}