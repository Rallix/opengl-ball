#version 430 core

in vec3 position;
in vec2 textureCoords;
in vec3 normal;

out vec2 pass_textureCoords;
out vec3 surfaceNormal; // normála
out vec3 toLightVector[2]; // směr ke světlu
out vec3 toCameraVector; // směr ke kameře
out float visibility; // viditelnost objektu (mlha)

uniform mat4 projectionMatrix;
uniform mat4 transformationMatrix;
uniform mat4 viewMatrix;

uniform vec3 lightPosition[2];
uniform float useFakeLighting;

const float gradient = 1.5;
uniform float fogDensity; // hustota mlhy


void main(void) {
    vec4 worldPosition = transformationMatrix * vec4(position, 1);
    vec4 positionFromCamera = viewMatrix * worldPosition;
    gl_Position = projectionMatrix * positionFromCamera; // aplikovat transformace
    pass_textureCoords = textureCoords;

    vec3 actualNormal = (useFakeLighting < 0.5f) ? normal : vec3(0,1,0); // falešné osvětlení -> falešná normála    

    surfaceNormal = (transformationMatrix * vec4(actualNormal, 0)).xyz; // aplikovat transformace na normálu
    for (int i = 0; i < 2; i++) {
        toLightVector[i] = lightPosition[i] - worldPosition.xyz; // cíl - zdroj
    }
    // toLightVector = lightPosition - worldPosition.xyz; // cíl - zdroj    
    vec3 cameraPosition = (inverse(viewMatrix) * vec4(0,0,0,1)).xyz; // pozice kamera je inverzní k pohledové matici
    toCameraVector = cameraPosition - worldPosition.xyz;

    // Mlha
    float distanceFromCamera = length(positionFromCamera.xyz);
    visibility = exp(-pow(distanceFromCamera * fogDensity, gradient)); // klesající křivka
    visibility = clamp(visibility, 0, 1);
}