/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shaders;

import entities.Camera;
import entities.Light;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import tools.Matrices;

/**
 *
 * @author Tom
 */
public class TerrainShader extends ShaderProgram {

    public static final String VERTEX_FILE = "src/shaders/terrainVertexShader.glsl";
    public static final String FRAGMENT_FILE = "src/shaders/terrainFragmentShader.glsl";

    private int projectionMatrixLoc;
    private int viewMatrixLoc;
    private int transformationMatrixLoc;

    private int lightPositionLoc;
    private int lightColourLoc;
    private int shineDamperLoc;
    private int reflectivityLoc;

    private int skyColourLoc;
    private int fogDensityLoc;

    private int backgroundTextureLoc;
    private int redTextureLoc;
    private int blueTextureLoc;
    private int greenTextureLoc;
    private int blendMapLoc;

    public TerrainShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        projectionMatrixLoc = super.getUniformLocation("projectionMatrix");
        viewMatrixLoc = super.getUniformLocation("viewMatrix");
        transformationMatrixLoc = super.getUniformLocation("transformationMatrix");

        lightPositionLoc = super.getUniformLocation("lightPosition");
        lightColourLoc = super.getUniformLocation("lightColour");

        shineDamperLoc = super.getUniformLocation("shineDamper");
        reflectivityLoc = super.getUniformLocation("reflectivity");

        skyColourLoc = super.getUniformLocation("skyColour");
        fogDensityLoc = super.getUniformLocation("fogDensity");

        backgroundTextureLoc = super.getUniformLocation("backgroundTexture");
        redTextureLoc = super.getUniformLocation("redTexture");
        blueTextureLoc = super.getUniformLocation("blueTexture");
        greenTextureLoc = super.getUniformLocation("greenTexture");
        blendMapLoc = super.getUniformLocation("blendMap");
    }

    /**
     * Načte projekční matici.
     *
     * @param projection Projekční matice.
     */
    public void loadProjectionMatrix(Matrix4f projection) {
        super.loadMatrix(projectionMatrixLoc, projection);
    }

    /**
     * Načte pohledovou matici.
     *
     * @param camera Používaná kamera.
     */
    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Matrices.createViewMatrix(camera);
        super.loadMatrix(viewMatrixLoc, viewMatrix);
    }

    /**
     * Načte transformační matici.
     *
     * @param transformation Transformační matice
     */
    public void loadTransformationMatrix(Matrix4f transformation) {
        super.loadMatrix(transformationMatrixLoc, transformation);
    }

    /**
     * Načte světlo.
     *
     * @param light Světlo.
     */
    public void loadLight(Light light) {
        super.loadVector(lightPositionLoc, light.getPosition());
        super.loadVector(lightColourLoc, light.getColour());
    }

    /**
     * Načte hodnoty odrazivosti materiálu.
     *
     * @param damper Tolerance kamery.
     * @param reflectivity Odrazivost materiálu.
     */
    public void loadShine(float damper, float reflectivity) {
        super.loadFloat(shineDamperLoc, damper);
        super.loadFloat(reflectivityLoc, reflectivity);

    }

    /**
     * Načte barvu oblohy (pozadí).
     *
     * @param red Červená složka barvy oblohy.
     * @param green Zelená složka barvy oblohy.
     * @param blue Modrá složka barvy oblohy.
     */
    public void loadSkyColour(float red, float green, float blue) {
        Vector3f skyColour = new Vector3f(red, green, blue);
        super.loadVector(skyColourLoc, skyColour);
    }

    /**
     * Načte hustotu mlhy.
     *
     * @param fogDensity Hustota mlhy.
     */
    public void loadFogDensity(float fogDensity) {
        super.loadFloat(fogDensityLoc, fogDensity);
    }

    /**
     * Přiřadí jednotlivé textury z balíku textur.
     */
    public void connectTextureUnits() {
        //! Pozor na správné pořadí
        super.loadInt(backgroundTextureLoc, 0);
        super.loadInt(redTextureLoc, 1);
        super.loadInt(blueTextureLoc, 2);
        super.loadInt(greenTextureLoc, 3);
        super.loadInt(blendMapLoc, 4);
    }

}
