/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shaders;

import entities.Camera;
import entities.Light;
import java.util.List;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import tools.Matrices;

/**
 *
 * @author Tom
 */
public class SurfaceShader extends ShaderProgram {

    private static final int MAX_LIGHTS = 2;

    public static final String VERTEX_FILE = "src/shaders/vertexShader.glsl";
    public static final String FRAGMENT_FILE = "src/shaders/fragmentShader.glsl";

    private int projectionMatrixLoc;
    private int viewMatrixLoc;
    private int transformationMatrixLoc;

    private int lightPositionLoc[];
    private int lightColourLoc[];
    private int shineDamperLoc;
    private int reflectivityLoc;
    private int useFakeLightingLoc;

    private int skyColourLoc;
    private int fogDensityLoc;

    public SurfaceShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        projectionMatrixLoc = super.getUniformLocation("projectionMatrix");
        viewMatrixLoc = super.getUniformLocation("viewMatrix");
        transformationMatrixLoc = super.getUniformLocation("transformationMatrix");

        shineDamperLoc = super.getUniformLocation("shineDamper");
        reflectivityLoc = super.getUniformLocation("reflectivity");

        useFakeLightingLoc = super.getUniformLocation("useFakeLighting");

        skyColourLoc = super.getUniformLocation("skyColour");
        fogDensityLoc = super.getUniformLocation("fogDensity");

        lightPositionLoc = new int[MAX_LIGHTS];
        lightColourLoc = new int[MAX_LIGHTS];
        for (int i = 0; i < MAX_LIGHTS; i++) {
            lightPositionLoc[i] = super.getUniformLocation("lightPosition[" + i + "]");
            lightColourLoc[i] = super.getUniformLocation("lightColour[" + i + "]");
        }
    }

    /**
     * Načte projekční matici.
     *
     * @param projection Projekční matice.
     */
    public void loadProjectionMatrix(Matrix4f projection) {
        super.loadMatrix(projectionMatrixLoc, projection);
    }

    /**
     * Načte pohledovou matici.
     *
     * @param camera Používaná kamera.
     */
    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Matrices.createViewMatrix(camera);
        super.loadMatrix(viewMatrixLoc, viewMatrix);
    }

    /**
     * Načte transformační matici.
     *
     * @param transformation Transformační matice
     */
    public void loadTransformationMatrix(Matrix4f transformation) {
        super.loadMatrix(transformationMatrixLoc, transformation);
    }

    /**
     * Načte světlo.
     *
     * @param light Světlo.
     */
    public void loadLights(List<Light> lights) {
        for (int i = 0; i < MAX_LIGHTS; i++) {
            if (i < lights.size()) {
                super.loadVector(lightPositionLoc[i], lights.get(i).getPosition());
                super.loadVector(lightColourLoc[i], lights.get(i).getColour());
            } else {
                super.loadVector(lightPositionLoc[i], new Vector3f(0, 0, 0));
                super.loadVector(lightColourLoc[i], new Vector3f(0, 0, 0));
            }
        }
    }

    /**
     * Načte hodnoty odrazivosti materiálu.
     *
     * @param damper Tolerance kamery.
     * @param reflectivity Odrazivost materiálu.
     */
    public void loadShine(float damper, float reflectivity) {
        super.loadFloat(shineDamperLoc, damper);
        super.loadFloat(reflectivityLoc, reflectivity);
    }

    /**
     * Zapne nebo vypne použití umělého osvětlení (normály směřují vzhůru).
     *
     * @param useFakeLighting Použít umělé světlo?
     */
    public void loadFakeLighting(boolean useFakeLighting) {
        super.loadBool(useFakeLightingLoc, useFakeLighting);
    }

    /**
     * Načte barvu oblohy (pozadí).
     *
     * @param red Červená složka barvy oblohy.
     * @param green Zelená složka barvy oblohy.
     * @param blue Modrá složka barvy oblohy.
     */
    public void loadSkyColour(float red, float green, float blue) {
        Vector3f skyColour = new Vector3f(red, green, blue);
        super.loadVector(skyColourLoc, skyColour);
    }

    /**
     * Načte hustotu mlhy.
     *
     * @param fogDensity Hustota mlhy.
     */
    public void loadFogDensity(float fogDensity) {
        super.loadFloat(fogDensityLoc, fogDensity);
    }

}
