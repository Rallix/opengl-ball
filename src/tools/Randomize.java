/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import static engine.Game.WORLD_RIM;
import java.util.Random;
import org.lwjgl.util.vector.Vector3f;
import terrains.Terrain;

/**
 *
 * @author Tom
 */
public class Randomize {

    /**
     * Vygeneruje náhodnou pozici na terénu.
     *
     * @param generator Generátor náhodných čísel
     * @param rim Minimální okraj terénu
     * @return Náhodná pozice na terénu
     */
    public static Vector3f terrainPosition(Random generator, Terrain terrain, float rim) {
        float limit = rim / Terrain.SIZE;
        float min = limit;
        float max = 1 - limit;

        float randomX = min + generator.nextFloat() * (max - min);
        float randomZ = min + generator.nextFloat() * (max - min);

        float terrainX = randomX * Terrain.SIZE;
        float terrainZ = randomZ * Terrain.SIZE;
        float terrainY = terrain.getHeightOfTerrain(terrainX, terrainZ);

        return new Vector3f(terrainX, terrainY, terrainZ);
    }

    public static Vector3f verticalRotation(Random generator, float maxAngle) {
        return new Vector3f(0, generator.nextFloat() * maxAngle, 0);
    }
}
