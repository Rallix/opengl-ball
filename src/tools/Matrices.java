/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import entities.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Tom
 */
public class Matrices {

    /**
     * Vytvoří tranformační matici ze zadaných transformací.
     *
     * @param translation Přesun po X, Y, Z osách.
     * @param rotation Rotace od X, Y, Z osy.
     * @param scale Změna velikosti po osách X, Y, Z naráz.
     * @return Výsledná transformační matice.
     */
    public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f rotation, float scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.setIdentity(); // jednotková matice: 1 na diagonále

        Matrix4f.translate(translation, matrix, matrix); // přesun -- zdroj -- cíl        

        Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1, 0, 0), matrix, matrix); // rotace po X
        Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0, 1, 0), matrix, matrix); // rotace po Y
        Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0, 0, 1), matrix, matrix); // rotace po Z

        Matrix4f.scale(new Vector3f(scale, scale, scale), matrix, matrix); // jednotná změna velikosti

        return matrix;
    }

    /**
     * Vytvoří pohledovou matici pro zadanou kameru.
     *
     * @param camera Používaná kamera.
     * @return Výsledná pohledová matice.
     */
    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
        Vector3f cameraPos = camera.getPosition();
        Vector3f negativeCameraPos = new Vector3f(-cameraPos.x, -cameraPos.y, -cameraPos.z);
        Matrix4f.translate(negativeCameraPos, viewMatrix, viewMatrix);
        return viewMatrix;
    }

    /**
     * Vrátí výšku na určité pozici v trojúhelníku dle jeho vrcholů.
     *
     * @param p1 Vrchol A.
     * @param p2 Vrchol B.
     * @param p3 Vrchol C.
     * @param pos Pozice v trojúhelníku.
     * @return Výška (Y).
     */
    public static float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
        float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }

}
